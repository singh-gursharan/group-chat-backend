defmodule GroupChat.Repo.Migrations.CreateChannels do
  use Ecto.Migration

  def change do
    create table(:channels) do
      add :name, :string
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps(inserted_at: :created_at)
    end
    create(unique_index(:channels, [:name, :user_id]))
  end
end
