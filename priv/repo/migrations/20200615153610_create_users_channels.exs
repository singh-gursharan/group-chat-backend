defmodule GroupChat.Repo.Migrations.CreateUsersChannels do
  use Ecto.Migration

  def change do
    create table(:users_channels) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :channel_id, references(:channels, on_delete: :delete_all), null: false

      timestamps(inserted_at: :created_at)
    end
    create(unique_index(:users_channels, [:user_id, :channel_id]))
    create index(:users_channels, [:user_id])
    create index(:users_channels, [:channel_id])
  end
end
