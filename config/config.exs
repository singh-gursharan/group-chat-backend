# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :group_chat,
  ecto_repos: [GroupChat.Repo]

# Configures the endpoint
config :group_chat, GroupChatWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "FoDHRdOcjRQrjksN1lt15I7xG8gw/PAgW2toq3pL514fBqL/HBg7VrQPqoWYaNtB",
  render_errors: [view: GroupChatWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: GroupChat.PubSub,
  live_view: [signing_salt: "pz/n7Bkm"]

config :guardian, Guardian.DB,
  # Add your repository module
  repo: GroupChat.Repo,
  # default
  schema_name: "guardian_tokens",
  sweep_interval: 60

config :group_chat, GroupChatWeb.Guardian,
  issuer: "GroupChat",
  secret_key: "MffJROH1lpLj3AEAvVzyv0iYgtON3qoZ5YSB+cDgd8nZDbdUMmGtq6wzWqhyAvSb
s",
  # optional
  allowed_algos: ["HS256"],
  ttl: {30, :days},
  allowed_drift: 2000,
  verify_issuer: true

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
