defmodule GroupChat.CMSTest do
  use GroupChat.DataCase

  alias GroupChat.CMS

  describe "admins" do
    alias GroupChat.CMS.Admin

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def admin_fixture(attrs \\ %{}) do
      {:ok, admin} =
        attrs
        |> Enum.into(@valid_attrs)
        |> CMS.create_admin()

      admin
    end

    test "list_admins/0 returns all admins" do
      admin = admin_fixture()
      assert CMS.list_admins() == [admin]
    end

    test "get_admin!/1 returns the admin with given id" do
      admin = admin_fixture()
      assert CMS.get_admin!(admin.id) == admin
    end

    test "create_admin/1 with valid data creates a admin" do
      assert {:ok, %Admin{} = admin} = CMS.create_admin(@valid_attrs)
    end

    test "create_admin/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = CMS.create_admin(@invalid_attrs)
    end

    test "update_admin/2 with valid data updates the admin" do
      admin = admin_fixture()
      assert {:ok, %Admin{} = admin} = CMS.update_admin(admin, @update_attrs)
    end

    test "update_admin/2 with invalid data returns error changeset" do
      admin = admin_fixture()
      assert {:error, %Ecto.Changeset{}} = CMS.update_admin(admin, @invalid_attrs)
      assert admin == CMS.get_admin!(admin.id)
    end

    test "delete_admin/1 deletes the admin" do
      admin = admin_fixture()
      assert {:ok, %Admin{}} = CMS.delete_admin(admin)
      assert_raise Ecto.NoResultsError, fn -> CMS.get_admin!(admin.id) end
    end

    test "change_admin/1 returns a admin changeset" do
      admin = admin_fixture()
      assert %Ecto.Changeset{} = CMS.change_admin(admin)
    end
  end

  describe "users_channels" do
    alias GroupChat.CMS.UserChannel

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def user_channel_fixture(attrs \\ %{}) do
      {:ok, user_channel} =
        attrs
        |> Enum.into(@valid_attrs)
        |> CMS.create_user_channel()

      user_channel
    end

    test "list_users_channels/0 returns all users_channels" do
      user_channel = user_channel_fixture()
      assert CMS.list_users_channels() == [user_channel]
    end

    test "get_user_channel!/1 returns the user_channel with given id" do
      user_channel = user_channel_fixture()
      assert CMS.get_user_channel!(user_channel.id) == user_channel
    end

    test "create_user_channel/1 with valid data creates a user_channel" do
      assert {:ok, %UserChannel{} = user_channel} = CMS.create_user_channel(@valid_attrs)
    end

    test "create_user_channel/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = CMS.create_user_channel(@invalid_attrs)
    end

    test "update_user_channel/2 with valid data updates the user_channel" do
      user_channel = user_channel_fixture()
      assert {:ok, %UserChannel{} = user_channel} = CMS.update_user_channel(user_channel, @update_attrs)
    end

    test "update_user_channel/2 with invalid data returns error changeset" do
      user_channel = user_channel_fixture()
      assert {:error, %Ecto.Changeset{}} = CMS.update_user_channel(user_channel, @invalid_attrs)
      assert user_channel == CMS.get_user_channel!(user_channel.id)
    end

    test "delete_user_channel/1 deletes the user_channel" do
      user_channel = user_channel_fixture()
      assert {:ok, %UserChannel{}} = CMS.delete_user_channel(user_channel)
      assert_raise Ecto.NoResultsError, fn -> CMS.get_user_channel!(user_channel.id) end
    end

    test "change_user_channel/1 returns a user_channel changeset" do
      user_channel = user_channel_fixture()
      assert %Ecto.Changeset{} = CMS.change_user_channel(user_channel)
    end
  end
end
