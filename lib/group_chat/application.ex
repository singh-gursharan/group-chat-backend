defmodule GroupChat.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      GroupChat.Repo,
      # Start the Telemetry supervisor
      GroupChatWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: GroupChat.PubSub},
      # Start the Endpoint (http/https)
      GroupChatWeb.Endpoint
      # Start a worker by calling: GroupChat.Worker.start_link(arg)
      # {GroupChat.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: GroupChat.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    GroupChatWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
