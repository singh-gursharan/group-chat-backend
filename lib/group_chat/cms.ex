defmodule GroupChat.CMS do
  @moduledoc """
  The CMS context.
  """

  import Ecto.Query, warn: false
  alias GroupChat.Repo
  alias GroupChat.CMS.UserChannel
  alias GroupChat.CMS.Admin
  alias GroupChat.Accounts.User

  @doc """
  Returns the list of admins.

  ## Examples

      iex> list_admins()
      [%Admin{}, ...]

  """
  def list_admins do
    Repo.all(Admin)
  end

  @doc """
  Gets a single admin.

  Raises `Ecto.NoResultsError` if the Admin does not exist.

  ## Examples

      iex> get_admin!(123)
      %Admin{}

      iex> get_admin!(456)
      ** (Ecto.NoResultsError)

  """
  def get_admin!(id), do: Repo.get!(Admin, id)

  @doc """
  Creates a admin.

  ## Examples

      iex> create_admin(%{field: value})
      {:ok, %Admin{}}

      iex> create_admin(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_admin(attrs \\ %{}) do
    %Admin{}
    |> Admin.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a admin.

  ## Examples

      iex> update_admin(admin, %{field: new_value})
      {:ok, %Admin{}}

      iex> update_admin(admin, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_admin(%Admin{} = admin, attrs) do
    admin
    |> Admin.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a admin.

  ## Examples

      iex> delete_admin(admin)
      {:ok, %Admin{}}

      iex> delete_admin(admin)
      {:error, %Ecto.Changeset{}}

  """
  def delete_admin(%Admin{} = admin) do
    Repo.delete(admin)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking admin changes.

  ## Examples

      iex> change_admin(admin)
      %Ecto.Changeset{data: %Admin{}}

  """
  def change_admin(%Admin{} = admin, attrs \\ %{}) do
    Admin.changeset(admin, attrs)
  end

  alias GroupChat.CMS.UserChannel

  @doc """
  Returns the list of users_channels.

  ## Examples

      iex> list_users_channels()
      [%UserChannel{}, ...]

  """
  def list_users_channels do
    Repo.all(UserChannel)
  end

  def list_user_channel_by_user(id) do
    query = from uc in UserChannel, where: uc.user_id == ^id
    Repo.all(query)
  end

  @doc """
  Gets a single user_channel.

  Raises `Ecto.NoResultsError` if the User channel does not exist.

  ## Examples

      iex> get_user_channel!(123)
      %UserChannel{}

      iex> get_user_channel!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user_channel!(id), do: Repo.get!(UserChannel, id)

  @doc """
  Creates a user_channel.

  ## Examples

      iex> create_user_channel(%{field: value})
      {:ok, %UserChannel{}}

      iex> create_user_channel(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user_channel(attrs \\ %{}) do
    %UserChannel{}
    |> UserChannel.changeset(attrs)
    |> Repo.insert()
  end
  def create_user_channel_multi(attrs \\ %{}) do
    IO.puts "in create_user_channel_multi"
    channel_id = attrs["channel_id"]
    user_id_list = attrs["user_ids"]
    channels_users = user_id_list
    |> Enum.map(fn x ->  (%UserChannel{} |> UserChannel.changeset(%{"channel_id" => channel_id, "user_id" => x}) |> Repo.insert()) end)
    |> Keyword.get_values(:ok)
    IO.inspect channels_users

    channels_users
  end

  @doc """
  Updates a user_channel.

  ## Examples

      iex> update_user_channel(user_channel, %{field: new_value})
      {:ok, %UserChannel{}}

      iex> update_user_channel(user_channel, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user_channel(%UserChannel{} = user_channel, attrs) do
    user_channel
    |> UserChannel.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user_channel.

  ## Examples

      iex> delete_user_channel(user_channel)
      {:ok, %UserChannel{}}

      iex> delete_user_channel(user_channel)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user_channel(%UserChannel{} = user_channel) do
    Repo.delete(user_channel)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user_channel changes.

  ## Examples

      iex> change_user_channel(user_channel)
      %Ecto.Changeset{data: %UserChannel{}}

  """
  def change_user_channel(%UserChannel{} = user_channel, attrs \\ %{}) do
    UserChannel.changeset(user_channel, attrs)
  end

  def list_users_by_channel(channel_id, %User{}) do
    subquery = from uc in UserChannel, where: uc.channel_id == ^channel_id, select: uc.user_id
    query = from u in User, where: u.id in subquery(subquery)
    query |> Repo.all() # will return a list of User changeset
  end

end
