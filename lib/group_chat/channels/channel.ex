defmodule GroupChat.Channels.Channel do
  use Ecto.Schema
  import Ecto.Changeset

  alias GroupChat.Accounts.User

  @required_fields ~w(name user_id)a
  schema "channels" do
    field :name, :string
    belongs_to(:admin, User, foreign_key: :user_id)

    has_many(:users, GroupChat.CMS.UserChannel)
    timestamps(inserted_at: :created_at)
  end

  @doc false
  def changeset(channel, attrs) do
    channel
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:name, name: :channels_name_user_id_index)
  end
end
