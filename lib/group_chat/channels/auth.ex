defmodule GroupChat.Channels.Auth do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, warn: false
  alias GroupChat.Repo

  alias GroupChat.Channels.Channel
  alias GroupChat.Accounts.User
  def is_admin(channel_id, user) do
    query = from c in Channel, where: c.id == ^channel_id and c.user_id == ^user.id
    Repo.exists?(query)
  end
end
