defmodule GroupChat.CMS.Admin do
  use Ecto.Schema
  import Ecto.Changeset

  schema "admins" do
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(admin, attrs) do
    admin
    |> cast(attrs, [])
    |> validate_required([])
    |> unique_constraint(:user_id)
  end
end
