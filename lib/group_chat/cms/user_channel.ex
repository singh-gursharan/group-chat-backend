defmodule GroupChat.CMS.UserChannel do
  use Ecto.Schema
  import Ecto.Changeset

  alias GroupChat.Accounts.User
  alias GroupChat.Channels.Channel

  @required_fields ~w(user_id channel_id)a

  schema "users_channels" do
    belongs_to(:user, User, foreign_key: :user_id)
    belongs_to(:channel, Channel, foreign_key: :channel_id)

    timestamps(inserted_at: :created_at)
  end

  @doc false
  def changeset(user_channel, attrs) do
    user_channel
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:user_id, name: :channels_user_id_article_id_index)
  end
end
