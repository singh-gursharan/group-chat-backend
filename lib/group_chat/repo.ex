defmodule GroupChat.Repo do
  use Ecto.Repo,
    otp_app: :group_chat,
    adapter: Ecto.Adapters.Postgres
end
