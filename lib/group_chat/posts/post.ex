defmodule GroupChat.Posts.Post do
  use Ecto.Schema
  import Ecto.Changeset
  alias GroupChat.Channels.Channel
  alias GroupChat.Accounts.User

  @required_fields ~w(message channel_id user_id)a
  schema "posts" do
    field :message, :string
    belongs_to(:channel, Channel, foreign_key: :channel_id)
    belongs_to(:user, User, foreign_key: :user_id)


    timestamps()
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
  end
end
