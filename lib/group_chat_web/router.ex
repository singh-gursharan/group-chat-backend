defmodule GroupChatWeb.Router do
  use GroupChatWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])

    plug(
      Guardian.Plug.Pipeline,
      error_handler: GroupChatWeb.SessionController,
      module: GroupChatWeb.Guardian
    )

    plug(Guardian.Plug.VerifyHeader)
    plug(Guardian.Plug.LoadResource, allow_blank: true)
  end

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/", GroupChatWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api", GroupChatWeb do
    pipe_through :api

    resources "/users", UserController, except: [:edit]
    get "/current/user", UserController, :current_user
    post("/users/login", SessionController, :create)
    get("users/:id/logout", SessionController, :logout)
    resources "/admins", AdminController, except: [:new, :edit]
    resources "/users/:id/channels", ChannelController, except: [:new, :edit]
    resources "/users/:id/channels/:channel_id/posts", PostController, except: [:new, :edit]
    post("/users/:id/channels/:channel_id/multi_users", ChannelUserController, :create_multi)

    resources "/users/:id/channels/:channel_id/users", ChannelUserController,
      except: [:new, :edit, :show, :update]

  end

  # Other scopes may use custom stacks.
  # scope "/api", GroupChatWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: GroupChatWeb.Telemetry
    end
  end
end
