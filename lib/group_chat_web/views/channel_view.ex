defmodule GroupChatWeb.ChannelView do
  use GroupChatWeb, :view
  alias GroupChatWeb.{ChannelView, UserView}

  def render("index.json", %{channels: channels}) do
    %{channels: render_many(channels, ChannelView, "channel.json")}
  end

  def render("show.json", %{channel: channel}) do
    %{channel: render_one(channel, ChannelView, "channel.json")}
  end

  def render("channel.json", %{channel: channel}) do
    channel
    |> Map.from_struct()
    # |> Map.put(:created_at, datetime_to_iso8601(channel.created_at))
    # |> Map.put(:updated_at, datetime_to_iso8601(channel.updated_at))
    |> Map.take([:name, :admin, :created_at, :updated_at, :id])
    |> Map.put(:admin, UserView.render("admin.json", user: channel.admin))


  end

  defp datetime_to_iso8601(datetime) do
    datetime
    |> Map.put(:microsecond, {elem(datetime.microsecond, 0), 3})
    |> DateTime.to_iso8601()
  end
end
