defmodule GroupChatWeb.UserView do
  use GroupChatWeb, :view
  alias GroupChatWeb.UserView

  def render("index.json", %{users: users}) do
    %{users: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user, jwt: jwt}) do
    %{user: Map.merge(render_one(user, UserView, "user.json"), %{token: jwt})}
  end

  def render("login.json", %{jwt: jwt, user: user}) do
    %{user: Map.merge(render_one(user, UserView, "user.json"), %{token: jwt})}
  end

  def render("logout.json",_) do
    %{logout: "success"}
  end

  def render("user.json", %{user: user}) do
    %{id: user.id, username: user.username, email: user.email }
  end

  def render("admin.json", %{user: user}) do
    %{id: user.id, username: user.username, email: user.email}
  end

  def render("error.json", %{message: message}) do
    %{message: message}
  end
end
