defmodule GroupChatWeb.PostView do
  use GroupChatWeb, :view
  alias GroupChatWeb.PostView
  alias GroupChatWeb.UserView

  def render("index.json", %{posts: posts}) do
    %{posts: render_many(posts, PostView, "post.json")}
  end

  def render("show.json", %{post: post}) do
    %{post: render_one(post, PostView, "post.json")}
  end

  def render("post.json", %{post: post}) do
    IO.inspect post.user
    post
    |> Map.from_struct()
    |> Map.take([:id, :message, :user])
    |> Map.put(:user, UserView.render("user.json", user: post.user))
  end
end
