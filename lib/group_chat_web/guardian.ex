defmodule GroupChatWeb.Guardian do
  @moduledoc """
  This module is required by Guardian, to implements all type or configuration for
  the auth token.

  Also, is used to retrieve and deliver the authentication subject to Guardian.

  More details here: https://github.com/ueberauth/guardian#installation
  """
  use Guardian, otp_app: :group_chat
  # @behaviour Guardian.Serializer
  alias GroupChat.{Repo, Accounts.User}

  def subject_for_token(%User{} = user, _claims), do: {:ok, to_string(user.id)}
  def subject_for_token(_, _), do: {:error, "Unknown resource type"}

  def resource_from_claims(%{"sub" => user_id}), do: {:ok, Repo.get(User, user_id)}
  def resource_from_claims(_claims), do: {:error, "Unknown resource type"}


  def after_encode_and_sign(resource, claims, token, _options) do
    with {:ok, _} <- Guardian.DB.after_encode_and_sign(resource, claims["typ"], claims, token) do
      {:ok, token}
    end
  end

  def on_verify(claims, token, _options) do
    with {:ok, _} <- Guardian.DB.on_verify(claims, token) do
      {:ok, claims}
    end
  end

  def on_refresh({old_token, old_claims}, {new_token, new_claims}, _options) do
    with {:ok, _, _} <- Guardian.DB.on_refresh({old_token, old_claims}, {new_token, new_claims}) do
      {:ok, {old_token, old_claims}, {new_token, new_claims}}
    end
  end

  def on_revoke(claims, token, _options) do
    with {:ok, _} <- Guardian.DB.on_revoke(claims, token) do
      {:ok, claims}
    end
  end
  # def for_token(user = %User{}), do: { :ok, "User:#{user.id}" }
  # def for_token(_), do: { :error, "Unknown resource type" }
  # def from_token("User:" <> id), do: {:ok, Repo.get(User, id)}
  # def from_token(_), do: {:error, "Unknown resource type"}
end
