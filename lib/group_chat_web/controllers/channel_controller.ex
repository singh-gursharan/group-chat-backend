defmodule GroupChatWeb.ChannelController do
  use GroupChatWeb, :controller
  use GroupChatWeb.GuardedController

  alias GroupChat.Repo
  alias GroupChat.Channels
  alias GroupChat.Channels.Channel

  action_fallback GroupChatWeb.FallbackController

  plug(
    Guardian.Plug.EnsureAuthenticated
    when action in [
           :create,
           :update,
           :delete
         ]
  )

  def index(conn, _params, user) do
    with channels <- Channels.list_channels_by_user(user.id) do
      channels =
      channels
      |> Repo.preload([:admin])

      render(conn, "index.json", channels: channels)
    end
  end

  def create(conn, %{"channel" => channel_params}, user) do
    with {:ok, %Channel{} = channel} <-
           Channels.create_channel(create_params(channel_params, user)) do
      channel =
        channel
        |> Repo.preload([:admin])

      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.channel_path(conn, :show, channel))
      |> render("show.json", channel: channel)
    end
  end

  def create_params(channel_params, user) do
    channel_params
    |> Map.merge(%{"user_id" => user.id})
  end

  def show(conn, %{"id" => id}) do
    channel = Channels.get_channel!(id)
    render(conn, "show.json", channel: channel)
  end

  def update(conn, %{"id" => id, "channel" => channel_params}, user) do
    channel = Channels.get_channel!(id)

    with {:ok, %Channel{} = channel} <- Channels.update_channel(channel, channel_params) do
      render(conn, "show.json", channel: channel)
    end
  end

  def delete(conn, %{"id" => id}, user) do
    channel = Channels.get_channel!(id)

    with {:ok, %Channel{}} <- Channels.delete_channel(channel) do
      send_resp(conn, :no_content, "")
    end
  end
end
