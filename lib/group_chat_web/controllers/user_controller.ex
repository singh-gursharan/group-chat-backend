defmodule GroupChatWeb.UserController do
  use GroupChatWeb, :controller

  use GroupChatWeb.GuardedController

  alias GroupChat.Accounts
  alias GroupChat.Accounts.{Auth, User}

  action_fallback GroupChatWeb.FallbackController

  plug(Guardian.Plug.EnsureAuthenticated when action in [:current_user, :update])

  def index(conn, _params, _) do
    users = Accounts.list_users()
    render(conn, "index.json", users: users)
  end

  def create(conn, %{"user" => user_params}, _) do
    case Auth.register(user_params) do
      {:ok, user} ->
        {:ok, jwt, _full_claims} =
          user |> GroupChatWeb.Guardian.encode_and_sign(%{}, token_type: :token)

        conn
        |> put_status(:created)
        |> render("show.json", jwt: jwt, user: user)

      {:error, changeset} ->
        render(conn, GroupChatWeb.ChangesetView, "error.json", changeset: changeset)
    end
  end


  def show(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    render(conn, "show.json", user: user)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    with {:ok, %User{} = user} <- Accounts.update_user(user, user_params) do
      render(conn, "show.json", user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)

    with {:ok, %User{}} <- Accounts.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end

  def current_user(conn, _params, user) do
    jwt = GroupChatWeb.Guardian.Plug.current_token(conn)
    IO.puts("jwt")
    IO.puts(jwt)

    if user != nil do
      render(conn, "show.json", jwt: jwt, user: user)
    else
      conn
      |> put_status(:not_found)
      |> render(GroupChatWeb.ErrorView, "404.json", [])
    end

    conn
    |> put_status(:ok)
    |> render("show.json", jwt: jwt, user: user)
  end
end
