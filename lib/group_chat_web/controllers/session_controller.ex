defmodule GroupChatWeb.SessionController do
  use GroupChatWeb, :controller

  alias GroupChat.Accounts.Auth

  action_fallback(GroupChatWeb.FallbackController)

  def create(conn, params) do
    case Auth.find_user_and_check_password(params) do
      {:ok, user} ->
        {:ok, jwt, _full_claims} =
          user |> GroupChatWeb.Guardian.encode_and_sign(%{}, token_type: :token)

        conn
        |> put_status(:created)
        |> render(GroupChatWeb.UserView, "login.json", jwt: jwt, user: user)

      {:error, message} ->
        conn
        |> put_status(401)
        |> render(GroupChatWeb.UserView, "error.json", message: message)
    end
  end

  def logout(conn, _) do
    jwt = GroupChatWeb.Guardian.Plug.current_token(conn)
    IO.puts jwt
    result = GroupChatWeb.Guardian.revoke(jwt)
    IO.inspect result
    conn |> render(GroupChatWeb.UserView, "logout.json")
  end

  @spec auth_error(Plug.Conn.t(), {any, any}, any) :: Plug.Conn.t()
  def auth_error(conn, {_type, _reason}, _opts) do
    IO.inspect(conn)

    conn
    |> put_status(:forbidden)
    |> render(GroupChatWeb.UserView, "error.json", message: "Not Authenticated")
  end
end
