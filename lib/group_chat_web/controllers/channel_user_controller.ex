defmodule GroupChatWeb.ChannelUserController do
  use GroupChatWeb, :controller
  use GroupChatWeb.GuardedController

  plug :put_view, GroupChatWeb.UserView
  alias GroupChat.Repo
  alias GroupChat.Channels
  alias GroupChat.Channels.Channel
  alias GroupChat.CMS
  alias GroupChat.CMS.UserChannel
  alias GroupChat.Channels.Auth

  action_fallback GroupChatWeb.FallbackController

  plug(
    Guardian.Plug.EnsureAuthenticated
    when action in [
           :index,
           :create,
           :delete
         ]
  )

  def index(conn, %{"channel_id" => channel_id}, user) do
    users = CMS.list_users_by_channel(channel_id, user)
    render(conn, "index.json", users: users)
  end

  # Adding a user to a perticular channel
  def create(conn, %{"user_channel" => user_channel_params}, user) do
    if Auth.is_admin(user_channel_params["channel_id"], user) do
      with {:ok, %UserChannel{} = user_channel} <-
             CMS.create_user_channel(user_channel_params) do
        user_channel =
          user_channel
          |> Repo.preload([:user])

        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.channel_path(conn, :create, user_channel))
        |> json(%{username: user_channel.user.username})
      end
    else
      {:error, :admin_unauthorized}
    end
  end

  def create_multi(conn, %{"channel_id"=> channel_id, "user_channel" => user_channel_params}, user) do
    if Auth.is_admin(user_channel_params["channel_id"], user) do
      with users_channels =
             CMS.create_user_channel_multi(user_channel_params) do
        IO.puts "in_with"
        user_channel =
          users_channels
          |> Repo.preload([:user])


        user_ids = user_channel |> Enum.map(fn x -> x.user.id end)

        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.channel_user_path(conn, :create_multi, user.id, channel_id))
        |> json(%{user_ids: user_ids})
      end
    else
      {:error, :admin_unauthorized}
    end
  end

  def delete(conn, %{"id" => id}, user) do
    channel = Channels.get_channel!(id)

    with {:ok, %Channel{}} <- Channels.delete_channel(channel) do
      send_resp(conn, :no_content, "")
    end
  end
end
