defmodule GroupChatWeb.PostController do
  use GroupChatWeb, :controller
  use GroupChatWeb.GuardedController

  alias GroupChat.Posts
  alias GroupChat.Posts.Post
  alias GroupChat.Repo

  action_fallback GroupChatWeb.FallbackController

  plug(
    Guardian.Plug.EnsureAuthenticated
    when action in [
           :index,
           :create,
           :update,
           :delete
         ]
  )

  def index(conn, %{"channel_id" => channel_id}, user) do
    with posts <- Posts.list_posts_by_channel(channel_id) do
      posts =
        posts
        |> Repo.preload([:user, :channel])

      render(conn, "index.json", posts: posts)
    end
  end

  def create(conn, %{"post" => post_params, "channel_id" => channel_id}, user) do
    IO.inspect create_params(post_params,user,channel_id)
    with {:ok, %Post{} = post} <- Posts.create_post(create_params(post_params, user, channel_id)) do
      post = post |> Repo.preload([:user, :channel])

      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.post_path(conn, :show, post, Any))
      |> render("show.json", post: post)
    end
  end

  defp create_params(post_params, user, channel_id) do
    post_params
    |> Map.merge(%{"user_id" => user.id, "channel_id" => channel_id})
  end

  def show(conn, %{"id" => id}) do
    post = Posts.get_post!(id)
    render(conn, "show.json", post: post)
  end

  def update(conn, %{"id" => id, "post" => post_params}) do
    post = Posts.get_post!(id)

    with {:ok, %Post{} = post} <- Posts.update_post(post, post_params) do
      render(conn, "show.json", post: post)
    end
  end

  def delete(conn, %{"id" => id}) do
    post = Posts.get_post!(id)

    with {:ok, %Post{}} <- Posts.delete_post(post) do
      send_resp(conn, :no_content, "")
    end
  end
end
