defmodule GroupChatWeb.AppGroupChannel do
  use GroupChatWeb, :channel
  alias GroupChat.{Posts, Channels, Repo}
  import Phoenix
  alias GroupChatWeb.PostView

  @impl true
  def join("channel:" <> channel_id, payload, socket) do
    if authorized?(payload) do
      # Here one more authentication needs to be done in
      # which user making the reques should be the part of
      # channel.
      IO.puts("in join")
      channel = Channels.get_channel!(channel_id)

      posts =
        Posts.list_posts_by_channel(channel_id)
        |> Repo.preload([:user, :channel])

      posts = Phoenix.View.render(GroupChatWeb.PostView, "index.json", posts: posts)
      IO.inspect(posts)
      socket = assign(socket, :channel, channel)
      {:ok, posts, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  @impl true
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (app_group:lobby).
  @impl true
  def handle_in("shout", payload, socket) do
    broadcast(socket, "shout", payload)
    {:noreply, socket}
  end

  def handle_in("response:update", %{"response" => codeResponse}, socket) do
    current_user = socket.assigns.current_user
    params = %{
      "channel_id" => socket.assigns.channel.id,
      "user_id" => current_user.id,
      "message" => codeResponse
    }

    case Posts.create_post(params) do
      {:ok, post} ->
        post = post |> Repo.preload([:user, :channel])
        post = Phoenix.View.render(GroupChatWeb.PostView, "show.json", post: post)

        broadcast!(socket, "response:updated", post)
        {:noreply, socket}

      {:error, changeset} ->
        {:reply, {:error, %{error: "Error updating challenge"}}, socket}
    end
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
